﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/BonusMaster.master" CodeBehind="Cartesian.aspx.cs" Inherits="BonusAssign.Cartesian" %>


 <asp:Content ContentPlaceHolderID="MainContent" runat="server">
    
  <div id="mainbody">             
    <h1> Cartesian Plane </h1>
    <label> X value:</label> <br />
    <asp:TextBox runat="server" id="xvalue"></asp:TextBox> <br />
    <asp:CustomValidator runat="server" ErrorMessage="Number cannot be zero." ControlToValidate="xvalue" OnServerValidate="Xval_Validator"></asp:CustomValidator> <br />
    
    <label> Y value:</label> <br />
    <asp:TextBox runat="server" id="yvalue"></asp:TextBox><br />
    <asp:CustomValidator runat="server" ErrorMessage="Number cannot be zero." ControlToValidate="yvalue" OnServerValidate="Yval_Validator"></asp:CustomValidator> <br />
    
    
    <asp:Button runat="server" ID="QuadButton" OnClick="QuadLoad" Text="Submit"/>
    
    <div runat="server" id="quadrant"></div>
  </div> 
 </asp:Content>


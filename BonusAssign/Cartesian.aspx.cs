﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssign
{

    public partial class Cartesian : System.Web.UI.Page
    {
        protected void Xval_Validator(object source, ServerValidateEventArgs args)
        {
            if (int.Parse(xvalue.Text) == 0){
                args.IsValid = false;

            }
            else {
                args.IsValid = true;
            }


        }

        protected void Yval_Validator(object source, ServerValidateEventArgs args)
        {
            if (int.Parse(yvalue.Text) == 0)
            {
                args.IsValid = false;

            }
            else
            {
                args.IsValid = true;
            }


        }
        protected void QuadLoad(object sender, EventArgs e)
        {

            if (!Page.IsValid)
            {
                return;
            }

            int num1 = int.Parse(xvalue.Text);
            int num2 = int.Parse(yvalue.Text);

            if (num1 > 0 && num2 > 0)
            {

                quadrant.InnerHtml = "The number is in the 1st quadrant";
            }
            else if (num1 < 0 && num2 > 0)
            {
                quadrant.InnerHtml = "The number is in the 2nd quadrant";
            }
            else if (num1 < 0 && num2 < 0)
            {
                quadrant.InnerHtml = "The number is in the the 3rd quadrant";
            }
            else
            {
                quadrant.InnerHtml = "The number is in the the 4th quadrant";
            }
        }
    }
}

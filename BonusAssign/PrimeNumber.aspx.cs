﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace BonusAssign
{

    public partial class PrimeNumber : System.Web.UI.Page
    {
        protected void Pnumload(object sender, EventArgs e)
        {

            if (!Page.IsValid)
            {
                return;
            }
            int pn = int.Parse(pnum.Text);
            int counter = 0;


       

        for (int i = 1; i <= pn; i++)
            {
                if (pn % i == 0)
                {
                    counter++;
                }

            }

            if (counter == 2)
            {
                pnprint.InnerHtml = pn + " is a prime number.";
            }

            else
            {
                pnprint.InnerHtml = pn + " is not a prime number.";
            }
        }


    }
}

